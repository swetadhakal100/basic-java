
//make a arrow function as input firstcapitalletter  as nitan   it must produce Nitan
export let firstLetterCapital = (name) => {
    let nameArr = name.split("")
    let _nameArr = nameArr.map((value, i) => {
        if (i === 0) {
            return value.toUpperCase()
        } else {
            return value.toLowerCase()
        }

    })

    let newString = _nameArr.join("")
    return newString
}

