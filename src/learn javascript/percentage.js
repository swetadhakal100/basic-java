export let _num = (percentage) => {
    if (percentage >= 90) {
        // console.log("GradeA")
        return "GradeA"
    } else if (percentage >= 80) {
        // console.log("GradeB")
        return "GradeB"
    } else if (percentage >= 70) {
        // console.log("GradeC")
        return "GradeC"
    } else if (percentage >= 60) {
        // console.log("GradeD")
        return "GradeD"
    } else if (percentage >= 50) {
        // console.log("GradeE")
        return "GradeE"
    } else if (percentage >= 40) {
        // console.log("GradeF")
        return "GradeG"
    }
}