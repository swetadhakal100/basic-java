import logo from './logo.svg';
import './App.css';
import { roll } from './abc';
import { age } from './age';
import { _age } from './greater';
import { number } from './room';
import { average } from './avg';
import { Age } from './ticket';
import { numbers } from './category';
import { n } from './oddeven';
import { _isAge } from './arrowage';
import { _isGreater } from './isgreater';
import { isroom } from './isroom';
import { multiply } from './learn javascript/multiply';
import { _num } from './learn javascript/percentage';
import { input } from './learn javascript/upperCase';
import { lowerCase } from './learn javascript/lowerCase';
import { trim } from './learn javascript/trim';
import { bearear } from './learn javascript/Bearear';
import { include } from './learn javascript/include';
import { replace } from './learn javascript/replace';
import {  stringSize } from './learn javascript/length';
import { func } from './learn javascript/niTan';
import { _string } from './learn javascript/string';
import { isAdminSuperAdmin } from './learn javascript/arrayMethod';
import { inputs } from './learn javascript/reverse';
import { inputActive } from './learn javascript/eitherPresent';
import { asc } from './learn javascript/asc';
import { myfunc} from './learn javascript/push';
import { desc } from './learn javascript/desc';
import { arrayChange } from './learn javascript/change';
import { strArr } from './learn javascript/isarray.js';
import { firstLetterCapital } from './learn javascript/firstCapitalletter';
import { PriceMultiply } from './learn javascript/arrayPrice';
import { productsType } from './learn javascript/arrayType';
import { productsId } from './learn javascript/aarrayId';
import { productsCategory } from './learn javascript/arrayCategory';
import { productsTitle } from './learn javascript/arrayTitle';
import { myfunct } from './learn javascript/myarray';
import { filename } from './learn javascript/file';









function App() {
  return (
    <div className="App">
      <header className="App-header">``
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

//homework1
/* console.log(roll)
console.log(age)
console.log(_age)
console.log(number)
average(1,2,3)
console.log(Age)
console.log(numbers)
console.log(n)
 */
//classwork2
//normal function
// console.log("nitan")
// let sum = function (a,b) {
//   let _sum =  a + b
//   console.log("hari")
//   return _sum
//   console.log("krishna")
// }
// console.log("thapa")
// let __sum = sum(1,2) 
// console.log(__sum)  


//arrow function 
// let a = () => {
//   return 1
// }
// let _a = a()
//  console.log(_a)


// let b = () => {
//   console.log("I am b")
// }
//  let a = () => {
//   console.log("I am a")
//   return b
//  }
//  let _a  = a()

//  _a()
//  b()


// let num1 = () => {
//   //java function can only return one value
//   return 1
// }
// console.log(num1())

//string literals
// let name = "nitan"
// let surName = "thapa"
// let fullName =  `my name is ${name} and surname is ${surName}`
// console.log(fullName)

// let name = "Sweta"
// let surName = "Dhakal"
// let address = "Gaushala"
// let fullName =  `my name is ${name}  surname is ${surName} and address is ${address}`
// console.log(fullName)

//homework2
// _isAge(18);
// _isGreater(19)
// isroom(19)

// let _mul = multiply(4,5)
// console.log(_mul)

// program with default parameter
// let defauultValue = (a,b=3) => {
//   console.log(a)
//   console.log(b)
// }
// defauultValue(1,2)
// javascript always execute code from top to bottom.

// let details = ['sweta', 'dhakal', 23, 24, true]
// console.log(details[2])
//  details[1] = thapa
//  console.log(details[1])

//homework3
// let __num =  _num(98)
// console.log(__num)
// _num(76)


//object
// let info = { name: 'sweta', surName: 'dhakal', age: 19 , isMarried: false }
// console.log(info)
// info.name = 'neha'
// console.log(info.name)
// delete info.surName
// console.log(info)
// info.roll = 425
// console.log(info)

//homework4
// let _input =  input("sweta")
// console.log(_input)

// let _lowerCase =  lowerCase("SWETA")
// console.log(_lowerCase)

// let _trim = trim("    sweta   ")
// console.log(_trim)

// let _bearear =  bearear("Token sweta")
// console.log(_bearear)

// let _include =   include("admin")
// console.log(_include)

// let _stringSize =  stringSize("sweta") 
// console.log(_stringSize)

// let _func = func("niTan")
// console.log(_func)

// let _replace =  replace("my name is Nitan thapa")
// console.log(_replace) 

//class
//conversion array to string
// let list = ["a", "nitan", "ram", 1]
// // join method convert array to string
// let newString = list.join("hari")
// console.log(newString)
// //conversion string to array
// let name = "abc gft fdjfjd"
// //split method convert string to array
// let newArray = name.split(" ")
// console.log(newArray)

// let newString =  _string("swetatadhakalswet")
// console.log(newString);

// let adminSuper = isAdminSuperAdmin("admin")
// console.log(adminSuper)

//homework5
// let _inputs =  inputs("sweta")
// console.log(_inputs)

// let _inputActive = inputActive("present")
// console.log(_inputActive)

//class
//type of operator
// console.log(typeof "ram");
// console.log(typeof 1);
// console.log(typeof true);

// console.log("a" - "b");

// let isEmpty  = (arr1) => {
//  if (arr1.length === 0){
//   return "true"
// }
// else {
//   return "false"
// }
// }
// let _isEmpty =  isEmpty(2,3)
// console.log(_isEmpty);

//foreach
// ["a", "b", "c"].forEach((value,i) => {
//   console.log("Hello");
// })

// ["a", "b", "c"].forEach((value,i) => {
//   console.log(value, i);
// })

//map
// let newmap =  ["a", "b", "c"].map((value,i) => {
//    return value
// })
// console.log(newmap);

// let newmap =  [1, 2, 3].map((value,i) => {
//    return value * 2
// })
// console.log(newmap);

//homework6
// let myarr = ["z", "f", "c", "e"]
// console.log(asc(myarr))

// let myarrdesc = ["z", "f", "c", "e"]
// console.log(desc(myarrdesc))


// let  _inputpush = [ 1, 2, 3 ]
// console.log(myfunc(_inputpush))

// let myinput = [1,2,3]
// console.log(arrayChange(myinput) )

// let mysplit = "my name is nitan thapa"
// console.log(strArr(mysplit))


//filter
// let fil = [1, 18, 19, 20].filter((value,i) => {
//   if (value === 18 || value >= 18) {
//     return true
//   } else {
//     return false
//   }
 
// } )
// console.log(fil);

//find    
//     let find = [1,2,3,4].find((value,i) => {
//    if(value >= 3) {
//     return true
//    }else{
//     return false
//    }

// })
// console.log(find)

//every
// let IsALLGreaterthan18 = [9,19,10].every( (value,i) => {
//  if (value === 9){
//   return true
//  }else {
//   return false
//  }
// })
// console.log( IsALLGreaterthan18)

//some
// let Issome = [9, 10, 20, 30].some( (value,i) => {
//   if (value >= 10){
//    return true
//   }else {
//    return false
//   }
//  })
//  console.log( Issome )

//make a arrow function that return true if one element of array is nitam
// let element = ["a", "b", "nitan"].some((value,i) => {
//   if (value === nitan){
//     return true
//   }else {
//     return false
//   }
// })
// console.log(element)

// let _element = ["a", "b", "nitan"].every((value,i) => {
//   if (value === nitan){
//     return true
//   }else {
//     return false
//   }
// })
// console.log(_element)

// let filter = ["nitam", "ram", "hari"].filter((value,i) => {
//       if ( value === nitan || value === ram) {
//         return true
//       }else{
//         return false
// }
// })
// console.log(filter)

// let _firstLetterCapital = firstLetterCapital("ram")
// console.log(_firstLetterCapital);

// let obj = { name: "nitan", age: 25, roll: 425 }
// let keysArray = Object.keys(obj)
// let valuesArray = Object.values(obj)
// console.log(keysArray)
// console.log(valuesArray)

//homework
// let products = [
//   {
//     id: 1,
//     title: "Product 1",
//     category: "electronics",
//     price: 5000,
//     description: "This is description and Product 1",
//     discount: {
//       type: "other",
//     },
//   },
//   {
//     id: 2,
//     title: "Product 2",
//     category: "cloths",
//     price: 2000,
//     description: "This is description and Product 2",
//     discount: {
//       type: "other",
//     },
//   },
//   {
//     id: 3,
//     title: "Product 3",
//     category: "electronics",
//     price: 3000,
//     description: "This is description and Product 3",
//     discount: {
//       type: "other",
//     },
//   },
// ]
// let categories = products.map ((value,i) => {
//       return value.category
// })

// let _categories = [...new Set(categories)]
// console.log(_categories)

// console.log(  productsId ( products ) )
// console.log( productsTitle( products ) )
// console.log( productsCategory ( products ) )
// console.log( productsType ( products ) )
// console.log( PriceMultiply(products) )
// console.log(priceGreate3000Title)


//throw error
// let error = new error("You have generated error")
// throw error
// error handling
//  
//Array operator: spread operator
//spread opperator is used to open the wrapper
// let ar1 = [1,2,3]
// let ar2 = ["a", "b", "c"]
// let arr3 = [...ar1,...ar2,5,6]
// console.log(arr3)

//for object
// let info1 = {name: "sweta", age: "23"}
// let info2 = {name: "hari", age: "30"}
// let info3 = {...info1,...info2}
// console.log(info3)

//set
//doesnot exist duplicate value
// let s = new Set([1,2,3,1])
// console.log(s)

// let S = [...new Set([1,2,3,1,1])]
// console.log(S)


// //remove duplicate value from array ['a', 'b', 'a', 'd']
// let duplicate = [...new Set['a', 'b', 'a', 'd']]
// console.log(duplicate)


// let _arr = [1,2,3]
// let  __arr = func(a=2, b=8, c=9, d=10){
//   return __arr
// }

//class

// let myar1 =  myfunct("sweta", "29", "address")
// console.log(myar1)

//setTimeout and setInterval
// setTimeout(()=>{
//   console.log("I am sweta")
//  }, 1000)
 
/*  setInterval(()=>{
  console.log("I am sweta")
 }, 5000) */

//class

// [1,2,3].reduce((prev,cur)=>{
//   return prev * cur
  
// },2)

 
//homework
// console.log(filename)

//classwork


// let {age,...info} = {name:'nitan', age: 29, roll: 30}
// console.log{age,info}


// let [age,...info] = ['nitan', 29, 30]
// console.log{age,info}

//reduce method
// let ar1 = [  {    name: "nitan",    gender: "male"  },  {    name: "sita",    gender: "female"  },  {    name: "hari",    gender: "male"  },  {    name: "gita",    gender: "female"  },  {    name: "utshab",    gender: "other"  }];

// const grouped = ar1.reduce((acc, cur) => {
//   if (!acc[cur.gender]) {
//     acc[cur.gender] = [];
//   }
//   acc[cur.gender].push(cur);
//   return acc;
// }, {});

// console.log(grouped);

//class
let sorts = [
 {
  id: 1,
  title: "Product 1",
  category: "electronics",
  price: 5000,
  description: "This is description and Product 1",
  discount: {
    type: "other",
  },
},
{
  id: 2,
  title: "Product 2",
  category: "cloths",
  price: 2000,
  description: "This is description and Product 2",
  discount: {
    type: "other",
  },
},
{
  id: 3,
  title: "Product 3",
  category: "electronics",
  price: 3000,
  description: "This is description and Product 3",
  discount: {
    type: "other",
  },
},
];

let newSort = sorts.sort((a,b)=>{

  return a.id-b.id

})
console.log(newSort)

let newSorts = sorts.sort((a,b)=>{

  return a.price-b.price

})
console.log(newSorts)



let isMarried= false
let hasCompletedBachelor=true
let info={
name:"nitan",
...isMarried?{spouseName:"someone"}:{},
...hasCompletedBachelor?{bachelor:"engineering in EC"}:{}
}
console.log(info);

























export default App;
